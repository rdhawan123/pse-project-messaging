﻿//-----------------------------------------------------------------------
// <author> 
//     Adrian McDonald Tariang (adrian47mcdonald@gmail.com)
// </author>
//
// <date> 
//     28th October, 2018
// </date>
// 
// <reviewer>
//      Libin N George (libinngeorge@gmail.com)
// </reviewer>
// 
// <copyright file="ConsoleLogger.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//  
// <summary>
//      Describe the Logger that prints to the Console.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using static Masti.QualityAssurance.CommandLineOptions;

    /// <summary>
    /// Define the functionality of the ConsoleLogger for the project.
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleLogger" /> class. 
        /// Sets local variables to permit specific types of log messages.
        /// </summary>
        /// <param name="messageTypes">Permitted type of Log messages.</param>
        public ConsoleLogger(IList<LogMessageType> messageTypes)
        {
            // Validate the messageTypes list
            if (messageTypes == null || messageTypes.Count() == 0)
            {
                messageTypes = new List<LogMessageType>()
                {
                    LogMessageType.Error,
                    LogMessageType.Success
                };
            }

            // Check if Info message should be printed.
            if (messageTypes.Contains(LogMessageType.Info))
            {
                this.AllowInfo = true;
            }

            // Check if Error message should be printed.
            if (messageTypes.Contains(LogMessageType.Error))
            {
                this.AllowError = true;
            }

            // Check if Success message should be printed.
            if (messageTypes.Contains(LogMessageType.Success))
            {
                this.AllowSuccess = true;
            }

            // Check if Warning message should be printed.
            if (messageTypes.Contains(LogMessageType.Warning))
            {
                this.AllowWarning = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to allow printing of Info messages.
        /// </summary>
        public bool AllowInfo { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether to allow printing of Info messages.
        /// </summary>
        public bool AllowError { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether to allow printing of Info messages.
        /// </summary>
        public bool AllowSuccess { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether to allow printing of Info messages.
        /// </summary>
        public bool AllowWarning { get; set; } = false;

        /// <summary>
        /// Logs the Error message to the Console.
        /// Checks if the current execution allows the logging of Error messages.
        /// </summary>
        /// <param name="message">the message to be the logged</param>
        public void LogError(string message)
        {
            if (this.AllowError)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"[Error]:  {message}");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Logs the Info message to the Console.
        /// Checks if the current execution allows the logging of Info messages.
        /// </summary>
        /// <param name="message">the message to be the logged</param>
        public void LogInfo(string message)
        {
            if (this.AllowInfo)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"[Info]:   {message}");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Logs the Success message to the Console.
        /// Checks if the current execution allows the logging of Success messages.
        /// </summary>
        /// <param name="message">the message to be the logged</param>
        public void LogSuccess(string message)
        {
            if (this.AllowSuccess)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"[Success]:{message}");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Logs the Warning message to the Console.
        /// Checks if the current execution allows the logging of Warning messages.
        /// </summary>
        /// <param name="message">the message to be the logged</param>
        public void LogWarning(string message)
        {
            if (this.AllowWarning)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"[Warning]:{message}");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Stub function as feature is not required for the Console Logger.
        /// </summary>
        public void MailLog()
        {
            // Feature does not exist for the Console
            string message = "[Info]\t Mail Feature is not available for the Console Logger";
            this.LogInfo(message);
        }
    }
}