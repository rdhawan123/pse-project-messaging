﻿//-----------------------------------------------------------------------
// <author> 
//     Amish Ranjan
// </author>
//
// <date> 
//     11-10-2018 
// </date>
// 
// <reviewer> 
//     Sooraj Tom 
// </reviewer>
// 
// <copyright file="Compression.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide option to compress and decompress image.
//      It uses diff technique to implement compression. The whole image is divided into
//      xDiv * yDiv(variables) parts, Compress returns only those parts which are different
//      from previous one. Decompress checks for key -1(full image) if found returns the 
//      whole image, else uses the previous image to return current image using same diff.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using Masti.QualityAssurance;
    using Newtonsoft.Json;

    /// <summary>
    /// This class provides option for compression and decompression of image.
    /// </summary>
    public class Compression : ICompression, IDisposable
    {
        /// <summary>
        /// Makes sure that a bitmap dictionary is created for further use if customized
        /// constructor for diff is called.
        /// </summary>
        private bool createPrevBmp = false;

        /// <summary>
        /// To be used on client side to store values corresponding to previous image.
        /// </summary>
        private Dictionary<int, byte[]> prevBmpDict = new Dictionary<int, byte[]>();

        /// <summary>
        /// To be used on server side to store values corresponding to previous image.
        /// </summary>
        private Bitmap prevBmp;

        /// <summary>
        /// Number of divisions of image on x axis and y axis, feel free to make changes.
        /// </summary>
        private int xDiv = 4, yDiv = 16;

        /// <summary>
        /// Track whether Dispose has been called.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Initializes a new instance of the Compression class
        /// </summary>
        public Compression()
        {
            //// Get instance which stores telemetry data across all modules. 
            ITelemetryCollector compressionTelemetryCollector = TelemetryCollector.Instance;

            //// Register telemetry.
            compressionTelemetryCollector.RegisterTelemetry("CompressionTelemetry", new CompressionTelemetry());
        }

        /// <summary>
        /// Gets or sets a value indicating whether createPrevBmp is set or not
        /// </summary>
        public bool CreatePrevBmp
        {
            get
            {
                return this.createPrevBmp;
            }

            set
            {
                this.createPrevBmp = value;
            }
        }

        /// <summary>
        /// It compresses the given Bitmap using diff technique.
        /// </summary>
        /// <param name="curBitmap">Bitmap for current image</param>
        /// <param name="implementDiff">Give true/false whether you want to implement 
        /// diff or not</param>
        /// <returns>
        /// A dictionary of int and Bitmap, with int representing the changed section 
        /// of screen if diff implemented
        /// </returns>
        public Dictionary<int, Bitmap> Compress(Bitmap curBitmap, bool implementDiff)
        {
            MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Compress: " +
                "Received for Compress"));

            Dictionary<int, Bitmap> bmpDict = new Dictionary<int, Bitmap>();

            if (curBitmap == null)
            {
                MastiDiagnostics.LogError(this.LogHelper("ImageProcessing.Compression.Compress:" +
                    " Invalid image passed"));
                bmpDict.Add(-2, null);
                return bmpDict;
            }

            int bmpPlace = 0;
            int widthBmp = curBitmap.Width;
            int heightBmp = curBitmap.Height;
            int[] recDim = new int[4];

            Bitmap divBmp;

            if (implementDiff == true && this.prevBmpDict.Count != this.xDiv * this.yDiv)
            {
                MastiDiagnostics.LogError(this.LogHelper("ImageProcessing.Compression.Compress:" +
                    " Invalid divisions of image"));
                bmpDict.Add(-2, null);
                return bmpDict;
            }
            else if (implementDiff == false && this.createPrevBmp == false)
            {
                MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Compress: " +
                    "Image Compressed/Processed succesfully and full image returned"));
                bmpDict.Add(-1, curBitmap);
                return bmpDict;
            }

            for (int x = 0; x < this.xDiv; ++x)
            {
                for (int y = 0; y < this.yDiv; ++y)
                {
                    recDim = this.GetRectangleDim(x, y, widthBmp, heightBmp);
                    Rectangle rect = new Rectangle(recDim[0], recDim[1], recDim[2], recDim[3]);
                    divBmp = curBitmap.Clone(rect, curBitmap.PixelFormat);
                    byte[] hashCode = this.GetHashCode(divBmp);
                    if (implementDiff == true && !hashCode.SequenceEqual(this.prevBmpDict[bmpPlace]))
                    {
                        this.prevBmpDict[bmpPlace] = hashCode;
                        bmpDict.Add(bmpPlace, divBmp);
                    }
                    else if (this.createPrevBmp == true)
                    {
                        if (this.prevBmpDict.ContainsKey(bmpPlace))
                        {
                            this.prevBmpDict[bmpPlace] = hashCode;
                        }
                        else
                        {
                            this.prevBmpDict.Add(bmpPlace, hashCode);
                        }
                    }

                    bmpPlace++;
                }
            }

            if (this.createPrevBmp == true)
            {
                bmpDict.Add(-1, curBitmap);
                MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Compress:" +
                    "Full image returned"));
            }
            else
            {
                ////performance factor in terms of number of divisions returned out of total
                CultureInfo culture;
                culture = CultureInfo.CreateSpecificCulture("eu-ES");
                MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Compress:" +
                    " Out of " + (this.xDiv * this.yDiv).ToString("G", culture) +
                    ", " + bmpDict.Count.ToString("G", culture) + " divisions returned"));

                ITelemetryCollector compressionTelemetryCollector = TelemetryCollector.Instance;

                CompressionTelemetry compressionTelemetry = (CompressionTelemetry)
                    compressionTelemetryCollector.GetTelemetryObject("CompressionTelemetry");

                compressionTelemetry.UpdatePerformance((1.0 - (bmpDict.Count / (this.xDiv * this.yDiv)))
                    .ToString("G", culture));
            }

            return bmpDict;
        }

        /// <summary>
        /// It decompresses the given Bitmap dictionary using diff technique.
        /// </summary>
        /// <param name="curBmpDict">Dictionary of int and Bitmap representing 
        /// compressed form of current Bitmap</param>
        /// <param name="implementDiff">Give true/false whether you want to implement 
        /// diff or not</param>
        /// <returns>A Bitmap of current image</returns>
        public Bitmap Decompress(Dictionary<int, Bitmap> curBmpDict, bool implementDiff)
        {
            MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Decompress: " +
                "Received for Decompress"));

            if (curBmpDict == null)
            {
                MastiDiagnostics.LogError(this.LogHelper("ImageProcessing.Compression.Decompress:" +
                    " Invalid dictionary received"));
                return null;
            }

            bool errorVal = false;
            if (curBmpDict.ContainsKey(-1))
            {
                this.prevBmp = new Bitmap(curBmpDict[-1]);
            }
            else
            {
                this.StitchImage(curBmpDict, ref errorVal);
            }

            if (errorVal == true)
            {
                MastiDiagnostics.LogError(this.LogHelper("ImageProcessing.Compression.Decompress:" +
                    " Error while stitching parts on previous image"));
                return null;
            }

            MastiDiagnostics.LogInfo(this.LogHelper("ImageProcessing.Compression.Decompress: " +
                "Image Decompressed succesfully"));
            return this.prevBmp;
        }

        /// <summary>
        /// This method helps to convert a dictionary of int, bitmap to json string
        /// </summary>
        /// <param name="bmpDict">A int, bitmap dictionary</param>
        /// <returns>string in json format</returns>
        public string BmpDictToString(Dictionary<int, Bitmap> bmpDict)
        {
            string json;
            json = JsonConvert.SerializeObject(bmpDict, new ImageConvert());
            return json;
        }

        /// <summary>
        /// This method helps to convert a json string back to dictionady of int, bitmap
        /// </summary>
        /// <param name="bmpDictString">Json string of dictionary of int, bitmap</param>
        /// <returns>Dictionary of int, bitmap</returns>
        public Dictionary<int, Bitmap> StringToBmpDict(string bmpDictString)
        {
            Dictionary<int, Bitmap> bmpDict;
            bmpDict = JsonConvert.DeserializeObject<Dictionary<int, Bitmap>>(bmpDictString, new ImageConvert());
            return bmpDict;
        }

        /// <summary>
        /// Checks two bitmaps if they are eual or not
        /// </summary>
        /// <param name="bmp1">First bitmap</param>
        /// <param name="bmp2">Second bitmap</param>
        /// <returns>Boolean value whether the bitmaps are equal or not</returns>
        public unsafe bool Equals(Bitmap bmp1, Bitmap bmp2)
        {
            if (bmp1 == null || bmp2 == null)
            {
                return false;
            }

            if (bmp1.Width != bmp2.Width || bmp1.Height != bmp2.Height)
            {
                return false;
            }

            Rectangle rc = new Rectangle(0, 0, bmp1.Width, bmp1.Height);
            BitmapData bd1 = bmp1.LockBits(rc, ImageLockMode.ReadOnly, bmp1.PixelFormat);
            BitmapData bd2 = bmp2.LockBits(rc, ImageLockMode.ReadOnly, bmp1.PixelFormat);
            bool retval = true;
            int* p1 = (int*)bd1.Scan0;
            int* p2 = (int*)bd2.Scan0;
            int cnt = bmp1.Height * bd1.Stride / 4;

            for (int ix = 0; ix < cnt; ++ix)
            {
                if (*p1++ != *p2++)
                {
                    retval = false;
                    break;
                }
            }

            bmp1.UnlockBits(bd1);
            bmp2.UnlockBits(bd2);
            return retval;
        }

        /// <summary>
        /// Public implementation of Dispose pattern.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">boolean to trigger disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                if (this.prevBmp != null)
                {
                    this.prevBmp.Dispose();
                }
            }

            this.disposed = true;
        }

        /// <summary>
        /// This method is a helper function of Decompress, it replaces a part of larger bitmap
        /// by smaller one.
        /// </summary>
        /// <param name="curBmpDict">A dictionary of int(representing place) and bitmap to 
        /// be replced in previous bitmap</param>
        /// <param name="errorVal">Changes value to true/false for error/success</param>
        private void StitchImage(Dictionary<int, Bitmap> curBmpDict, ref bool errorVal)
        {
            int x, y;
            int[] recDim = new int[4];
            Bitmap updatedBmp = this.prevBmp;
            foreach (var smallBmpPair in curBmpDict)
            {
                x = smallBmpPair.Key / this.yDiv;
                y = smallBmpPair.Key % this.yDiv;
                recDim = this.GetRectangleDim(x, y, updatedBmp.Width, updatedBmp.Height);
                try
                {
                    Graphics g = Graphics.FromImage(updatedBmp);
                    g.DrawImageUnscaled(smallBmpPair.Value, recDim[0], recDim[1]);
                    g.Dispose();
                }
                catch (ArgumentNullException)
                {
                    errorVal = true;
                    return;
                }
            }

            this.prevBmp = updatedBmp;
        }

        /// <summary>
        /// Gets MD5 hashcode for a given bitmap
        /// </summary>
        /// <param name="bmp">Bitmap for which we want hashcode</param>
        /// <returns>Hash code for given bitmap</returns>
        private byte[] GetHashCode(Bitmap bmp)
        {
            byte[] bytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                bmp.Save(ms, ImageFormat.Jpeg);
                bytes = ms.ToArray();
            }

            // hash the bytes
#pragma warning disable CA5351 // Do Not Use Broken Cryptographic Algorithms
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
#pragma warning restore CA5351 // Do Not Use Broken Cryptographic Algorithms
            {
                byte[] hash = md5.ComputeHash(bytes);
                return hash;
            }
        }

        /// <summary>
        /// This method calculates the parameters of reactangle, given x and y
        /// (x,y) = (0,yDiv) for top left and (x,y) = (xDiv, 0) for bottom right
        /// </summary>
        /// <param name="x">X axis value for bitmap</param>
        /// <param name="y">Y axis value for bitmap</param>
        /// <param name="width">Width for whole bitmap</param>
        /// <param name="height">Height for whole bitmap</param>
        /// <returns>Integer array of top left x and y coordinate of rectange and 
        /// its width and height</returns>
        private int[] GetRectangleDim(int x, int y, int width, int height)
        {
            int recTopLeftX, recTopLeftY, recWidth, recHeight;
            int widthDiv = width / this.xDiv;
            int heightDiv = height / this.yDiv;

            recTopLeftX = x * widthDiv;
            recTopLeftY = y * heightDiv;
            if (x == this.xDiv - 1)
            {
                recWidth = width - (x * widthDiv);
            }
            else
            {
                recWidth = widthDiv;
            }

            if (y == this.yDiv - 1)
            {
                recHeight = height - (y * heightDiv);
            }
            else
            {
                recHeight = heightDiv;
            }

            int[] recDim = new int[4] { recTopLeftX, recTopLeftY, recWidth, recHeight };
            return recDim;
        }

        /// <summary>
        /// helps to bypass log warning
        /// </summary>
        /// <param name="log">the value to be returned</param>
        /// <returns>same log value</returns>
        private string LogHelper(string log)
        {
            return log;
        }
    }
}
