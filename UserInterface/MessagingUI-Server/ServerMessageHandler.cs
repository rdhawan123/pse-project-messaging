﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Handle messages sent and received.
// </summary>
// <copyright file="ServerMessageHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Globalization;
    using System.Windows.Forms;
    using Masti.QualityAssurance;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Receiver message handler.
        /// </summary>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="toIP">The to IP<see cref="string"/></param>
        /// <param name="fromIP">The from IP<see cref="string"/></param>
        /// <param name="dateTime">The date time<see cref="string"/></param>
        public void ReceiverMessageHandler(string message, string toIP, string fromIP, string dateTime)
        {
            this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), fromIP, message, true, true);
        }

        /// <summary>
        /// Status handler for sent messages.
        /// </summary>
        /// <param name="status">The status<see cref="Messenger.Handler.StatusCode"/></param>
        /// <param name="fromIP">The from IP<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public void SendingStatusHandlers(Messenger.Handler.StatusCode status, string fromIP, string message)
        {
            if (status.ToString().Equals("Success", StringComparison.Ordinal)) // Message sent successfully.
            {
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), fromIP, message, false, true);
            }
            else // Message not sent.
            {
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), fromIP, message, false, false);
            }
        }

        /// <summary>
        /// Invoke message.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">Checks if it is client message or server message<see cref="bool"/></param>
        /// <param name="isSuccess">Success status<see cref="bool"/></param>
        public void InvokeMessage(string clientIP, string message, bool isClientMessage, bool isSuccess)
        {
            TabPage messagePage = null;
            messagePage = this.GetTabPage(clientIP + "tabPage");
            TextBox unreadMessageCountTextBox = null;
            TextBox tabPageTop = null;
            SplitContainer splitContainer = null;
            Control.ControlCollection messagePageControls = null;

            int tabPageNextTop = 0;

            if (messagePage == null)
            //// New client create a tab page and other controls. 
            {
                messagePage = this.CreateMessagePage(clientIP);
                splitContainer = this.CreateSplitContainer(clientIP);
                messagePage.Controls.Add(splitContainer);
                ServerChatSectionTabs.TabPages.Add(messagePage);
            }

            messagePageControls = messagePage.Controls;
            splitContainer = GetControl(messagePageControls, clientIP + "split") as SplitContainer;
            unreadMessageCountTextBox = GetControl(messagePageControls, clientIP + "unreadMessage") as TextBox;
            tabPageTop = GetControl(messagePageControls, clientIP + "tabPageTop") as TextBox;
            tabPageNextTop = int.Parse(tabPageTop.Text, CultureInfo.InvariantCulture);

            //// Client with latest message pushed to top. 
            this.ReorderTabs(messagePage);

            UpdateUnreadMessageCount(unreadMessageCountTextBox, isClientMessage);

            string tabPageNewHeight = this.CreateMessageTextBox(isClientMessage, tabPageNextTop, messagePage, message, splitContainer, isSuccess);
            tabPageTop.Text = tabPageNewHeight;
        }

        /// <summary>
        /// Send message function.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SendMessage(object sender, EventArgs e)
        {
            string systemTime = GetCurrentDateTime();
            try
            {
                string clientIP = GetClientName(ServerChatSectionTabs.SelectedTab.Name);

                if (messageInFocus ||  serverMessageTextBox.Focused) // Send message.
                {
                    this.messageHandler.SendMessage(serverMessageTextBox.Text, clientIP, systemTime);
                    serverMessageTextBox.Text = string.Empty;
                }
            }
            catch (Exception exception)
            {
                MastiDiagnostics.LogWarning("No client to send message");
                MessageBox.Show("No client to send message", "OK", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
