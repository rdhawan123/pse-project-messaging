﻿//-----------------------------------------------------------------------
// <author>
//      A Vinay Krishna
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Create UI controls for screen share and handle screen sharing.
// </summary>
// <copyright file="ServerImageHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.ImageProcessing;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Invoke image function.
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="ImageEventArgs"/></param>
        public void InvokeImage(object source, ImageEventArgs e)
        {
            string clientIP = e.ClientIP;
            string currentMessagePage = clientIP + "tabPage";
            TabPage messagePage = this.GetTabPage(currentMessagePage);
            SplitContainer splitContainer = GetControl(messagePage.Controls, clientIP + "split") as SplitContainer;
            PictureBox currentImageBox = GetControl(splitContainer.Panel1.Controls, clientIP + "pictureBox") as PictureBox;
            TextBox currentTime = GetControl(splitContainer.Panel1.Controls, clientIP + "timeStamp") as TextBox;

            //// Set image and time.
            currentImageBox.Image = e.ImageBitMap;
            currentTime.Text = GetCurrentDateTime();
        }

        /// <summary>
        /// Image sub scriber.
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="ImageEventArgs"/></param>
        public void ImageSubscriber(object source, ImageEventArgs e)
        {
            this.BeginInvoke(new InvokeImageDelegate(this.InvokeImage), source, e);
        }

        /// <summary>
        /// Image error handler.
        /// </summary>
        /// <param name="sender">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="Masti.ImageProcessing.ErrorEventArgs"/></param>
        public void ImageErrorHandler(object sender, Masti.ImageProcessing.ErrorEventArgs e)
        {
            if (e.StopSignalOnError == true)
            {
                MessageBox.Show("Connection Broken", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(e.ErrorMessage, "OK", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Error sharing.
        /// </summary>
        /// <param name="sender">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="Masti.ImageProcessing.ErrorEventArgs"/></param>
        public void ErrorSharing(object sender, Masti.ImageProcessing.ErrorEventArgs e)
        {

            this.BeginInvoke(new ImageErrorHandlerDelegate(ImageErrorHandler), sender, e);
        }

        /// <summary>
        /// Creates text box for time stamp.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <returns>Text box<see cref="TextBox"/></returns>
        private static TextBox CreateTimeStamp(string clientIP)
        {
            TextBox timeStamp = new TextBox();

            //// Setting properties for the time stamp text box.
            timeStamp.Name = clientIP + "timeStamp";
            timeStamp.Text = clientIP;
            timeStamp.BorderStyle = BorderStyle.None;
            timeStamp.Height = 100;
            timeStamp.WordWrap = true;
            timeStamp.Visible = false;
            timeStamp.BackColor = Color.Black;
            timeStamp.ForeColor = Color.White;
            timeStamp.TextAlign = HorizontalAlignment.Center;

            return timeStamp;
        }

        /// <summary>
        /// Creates picture box for screen sharing.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <param name="height">The height of the picture box<see cref="float"/></param>
        /// <returns>Picture box<see cref="PictureBox"/></returns>
        private PictureBox CreateScreenShareImageBox(string clientIP, float height)
        {
            PictureBox screenShareImageBox = new PictureBox();

            //// Setting properties for the screen share picture box.
            screenShareImageBox.Name = clientIP + "pictureBox";
            screenShareImageBox.Text = clientIP + "pictureBox";
            screenShareImageBox.Height = (int)height;
            screenShareImageBox.SizeMode = PictureBoxSizeMode.StretchImage;
            screenShareImageBox.Width = this.ServerChatSectionTabs.Width;
            screenShareImageBox.Visible = false;

            return screenShareImageBox;
        }
    }
}
