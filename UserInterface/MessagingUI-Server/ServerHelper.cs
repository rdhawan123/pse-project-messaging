﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Some helper functions
// </summary>
// <copyright file="ServerHelper.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Globalization;
    using System.Resources;
    using System.Windows.Forms;
    using Masti.QualityAssurance;


    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// The established connection.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <param name="clientUserName">The client user name<see cref="string"/></param>
        public void EstablishConnection(string clientIP, string clientUserName)
        {
            if (this.userNameIPMap.ContainsKey(clientIP)) // Update client IP with new user name.
            {
                this.userNameIPMap[clientIP] = clientUserName;
            }
            else // Add a new client.
            {
                this.userNameIPMap.Add(clientIP, clientUserName);
            }
        }

        /// <summary>
        /// Gets client name.
        /// </summary>
        /// <param name="client">The client<see cref="string"/></param>
        /// <returns>Client name <see cref="string"/></returns>
        private static string GetClientName(string client)
        {
            return client.Substring(0, client.Length - 7);
        }

        /// <summary>
        /// Gets current date and time.
        /// </summary>
        /// <returns>The date and time<see cref="string"/></returns>
        private static string GetCurrentDateTime()
        {
            string time = DateTime.Now.ToString("HH:mm tt", CultureInfo.InvariantCulture);
            DateTime dt = DateTime.Parse(time, CultureInfo.InvariantCulture);
            time = dt.ToString("HH:mm", CultureInfo.InvariantCulture);
            string date = DateTime.Now.ToString("MM/dd", CultureInfo.InvariantCulture);
            time = date + " " + time;

            return time;
        }

        /// <summary>
        /// Checks if the characters entered for port number are digits.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="KeyPressEventArgs"/></param>
        private void PortNumberCharacterEntry(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        /// <summary>
        /// Leave focus event of the text box to handle text place holder of the reply message box.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ServerMessageTextBoxLeaveFocus(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(serverMessageTextBox.Text))
            {
                serverMessageTextBox.Text = "Enter reply here...";
                this.messageInFocus = false; // No message in text box.
            }
            else
            {
                this.messageInFocus = true; // A message is present in text box.
            }
        }

        /// <summary>
        /// Enter focus event of the text box to handle text place holder of the reply message box.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ServerMessageTextBoxEnterFocus(object sender, EventArgs e)
        {
            if (!this.messageInFocus)
            {
                serverMessageTextBox.Text = string.Empty;
            }
        }

        /// <summary>
        /// Server chat screen on load function.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ServerChatScreenLoad(object sender, EventArgs e)
        {
            shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");

            // Do something
            string currentSessionID = "ABCD!@#$";
            toolStripLabel.Text = "Current Session ID: " + currentSessionID;
        }
        /// <summary>
        /// Server chat screen on close function.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="FormClosingEventArgs"/></param>
        private void ServerChatScreenFormClosing(object sender, FormClosingEventArgs e)
        {
            ITelemetryCollector telemetryCollector = TelemetryCollector.Instance;
            telemetryCollector.StoreTelemetry();

            //// Save session.
            ///



            string fileName = "Something.txt";
            MastiDiagnostics.LogInfo("Session saved to file: " + fileName);
            MessageBox.Show("Session saved to file: " + fileName, "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Environment.Exit(Environment.ExitCode);
        }
    }
}
