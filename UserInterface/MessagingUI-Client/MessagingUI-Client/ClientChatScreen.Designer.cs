﻿namespace Masti.MessagingUIClient
{
    partial class ClientChatScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.logoPanel = new System.Windows.Forms.Panel();
            this.errorLabel = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.clientIPTextBox = new System.Windows.Forms.TextBox();
            this.clientIPLabel = new System.Windows.Forms.Label();
            this.serverPortTextBox = new System.Windows.Forms.TextBox();
            this.serverPortLabel = new System.Windows.Forms.Label();
            this.serverIPTextBox = new System.Windows.Forms.TextBox();
            this.clientNameLabel = new System.Windows.Forms.Label();
            this.clientNameTextBox = new System.Windows.Forms.TextBox();
            this.serverIPLabel = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chatScreenTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.messageTestBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.GrayText;
            this.splitContainer1.Panel1.Controls.Add(this.logoPanel);
            this.splitContainer1.Panel1.Controls.Add(this.errorLabel);
            this.splitContainer1.Panel1.Controls.Add(this.connectButton);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(890, 636);
            this.splitContainer1.SplitterDistance = 268;
            this.splitContainer1.TabIndex = 0;
            // 
            // logoPanel
            // 
            this.logoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.logoPanel.Location = new System.Drawing.Point(0, 0);
            this.logoPanel.Name = "logoPanel";
            this.logoPanel.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.logoPanel.Size = new System.Drawing.Size(268, 100);
            this.logoPanel.TabIndex = 0;
            // 
            // errorLabel
            // 
            this.errorLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.errorLabel.AutoSize = true;
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(41, 322);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 17);
            this.errorLabel.TabIndex = 1;
            // 
            // connectButton
            // 
            this.connectButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.connectButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Location = new System.Drawing.Point(85, 364);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(99, 40);
            this.connectButton.TabIndex = 1;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = false;
            this.connectButton.Click += new System.EventHandler(this.ConnectButtonClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.clientIPTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.clientIPLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.serverPortTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.serverPortLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.serverIPTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.clientNameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.clientNameTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.serverIPLabel, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-8, 147);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(275, 192);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // clientIPTextBox
            // 
            this.clientIPTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.clientIPTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientIPTextBox.Location = new System.Drawing.Point(140, 147);
            this.clientIPTextBox.Name = "clientIPTextBox";
            this.clientIPTextBox.Size = new System.Drawing.Size(132, 28);
            this.clientIPTextBox.TabIndex = 0;
            // 
            // clientIPLabel
            // 
            this.clientIPLabel.AutoSize = true;
            this.clientIPLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.clientIPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientIPLabel.Location = new System.Drawing.Point(3, 144);
            this.clientIPLabel.Name = "clientIPLabel";
            this.clientIPLabel.Size = new System.Drawing.Size(131, 24);
            this.clientIPLabel.TabIndex = 0;
            this.clientIPLabel.Text = "Client IP";
            this.clientIPLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // serverPortTextBox
            // 
            this.serverPortTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.serverPortTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverPortTextBox.Location = new System.Drawing.Point(140, 99);
            this.serverPortTextBox.Name = "serverPortTextBox";
            this.serverPortTextBox.Size = new System.Drawing.Size(132, 28);
            this.serverPortTextBox.TabIndex = 3;
            // 
            // serverPortLabel
            // 
            this.serverPortLabel.AutoSize = true;
            this.serverPortLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.serverPortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverPortLabel.Location = new System.Drawing.Point(3, 96);
            this.serverPortLabel.Name = "serverPortLabel";
            this.serverPortLabel.Size = new System.Drawing.Size(131, 24);
            this.serverPortLabel.TabIndex = 3;
            this.serverPortLabel.Text = "Server Port";
            this.serverPortLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // serverIPTextBox
            // 
            this.serverIPTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.serverIPTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverIPTextBox.Location = new System.Drawing.Point(140, 51);
            this.serverIPTextBox.Name = "serverIPTextBox";
            this.serverIPTextBox.Size = new System.Drawing.Size(132, 28);
            this.serverIPTextBox.TabIndex = 2;
            // 
            // clientNameLabel
            // 
            this.clientNameLabel.AutoSize = true;
            this.clientNameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameLabel.Location = new System.Drawing.Point(3, 0);
            this.clientNameLabel.Name = "clientNameLabel";
            this.clientNameLabel.Size = new System.Drawing.Size(131, 24);
            this.clientNameLabel.TabIndex = 2;
            this.clientNameLabel.Text = "Client Name";
            this.clientNameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // clientNameTextBox
            // 
            this.clientNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clientNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameTextBox.Location = new System.Drawing.Point(140, 3);
            this.clientNameTextBox.Name = "clientNameTextBox";
            this.clientNameTextBox.Size = new System.Drawing.Size(132, 27);
            this.clientNameTextBox.TabIndex = 1;
            // 
            // serverIPLabel
            // 
            this.serverIPLabel.AutoSize = true;
            this.serverIPLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.serverIPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverIPLabel.Location = new System.Drawing.Point(3, 48);
            this.serverIPLabel.Name = "serverIPLabel";
            this.serverIPLabel.Size = new System.Drawing.Size(131, 24);
            this.serverIPLabel.TabIndex = 1;
            this.serverIPLabel.Text = "Server IP";
            this.serverIPLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chatScreenTableLayout);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(0, 15, 15, 10);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(618, 636);
            this.splitContainer2.SplitterDistance = 584;
            this.splitContainer2.TabIndex = 0;
            // 
            // chatScreenTableLayout
            // 
            this.chatScreenTableLayout.BackColor = System.Drawing.Color.RosyBrown;
            this.chatScreenTableLayout.ColumnCount = 1;
            this.chatScreenTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.chatScreenTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatScreenTableLayout.Location = new System.Drawing.Point(0, 15);
            this.chatScreenTableLayout.Name = "chatScreenTableLayout";
            this.chatScreenTableLayout.RowCount = 1;
            this.chatScreenTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.chatScreenTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.chatScreenTableLayout.Size = new System.Drawing.Size(603, 559);
            this.chatScreenTableLayout.TabIndex = 0;
            this.chatScreenTableLayout.SizeChanged += new System.EventHandler(this.ChatScreenTableLayoutSizeChanged);
            this.chatScreenTableLayout.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.ChatScreenTableLayoutControlAdded);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.messageTestBox);
            this.splitContainer3.Panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitContainer3.Panel1.Padding = new System.Windows.Forms.Padding(0, 5, 5, 10);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.sendButton);
            this.splitContainer3.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitContainer3.Panel2.Margin = new System.Windows.Forms.Padding(3);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(5, 5, 15, 10);
            this.splitContainer3.Size = new System.Drawing.Size(618, 48);
            this.splitContainer3.SplitterDistance = 558;
            this.splitContainer3.TabIndex = 0;
            // 
            // messageTestBox
            // 
            this.messageTestBox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.messageTestBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.messageTestBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageTestBox.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.messageTestBox.Location = new System.Drawing.Point(0, 5);
            this.messageTestBox.Multiline = true;
            this.messageTestBox.Name = "messageTestBox";
            this.messageTestBox.Size = new System.Drawing.Size(553, 33);
            this.messageTestBox.TabIndex = 0;
            this.messageTestBox.Text = "Enter Text Here";
            this.messageTestBox.Enter += new System.EventHandler(this.MessageTestBoxEnter);
            // 
            // sendButton
            // 
            this.sendButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendButton.Location = new System.Drawing.Point(5, 5);
            this.sendButton.Margin = new System.Windows.Forms.Padding(0);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(36, 33);
            this.sendButton.TabIndex = 0;
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.SendButtonClick);
            // 
            // ClientChatScreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(890, 636);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(350, 0);
            this.Name = "ClientChatScreen";
            this.Text = "Client-UI";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TextBox messageTestBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label serverPortLabel;
        private System.Windows.Forms.Label clientNameLabel;
        private System.Windows.Forms.Label serverIPLabel;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox serverPortTextBox;
        private System.Windows.Forms.TextBox serverIPTextBox;
        private System.Windows.Forms.TextBox clientNameTextBox;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.TableLayoutPanel chatScreenTableLayout;
        private System.Windows.Forms.Panel logoPanel;
        private System.Windows.Forms.TextBox clientIPTextBox;
        private System.Windows.Forms.Label clientIPLabel;
    }
}

