﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya 
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     K Durga Prasad Reddy
// </reviewer>
// 

// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Net;
    using System.Net.Sockets;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Gets the Date and Time from the Machine
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        internal static string GetDateTime()
        {
            string systemTime = DateTime.Now.ToString("HH:mm tt", null);
            DateTime dt = DateTime.Parse(systemTime, null);
            systemTime = dt.ToString("HH:mm", null);
            string date = DateTime.Now.ToString("MM/dd", null);
            systemTime = date + " " + systemTime;

            return systemTime;
        }

        /// <summary>
        /// Changes the Text and Enable property of the connectButton
        /// </summary>
        public void ChangeConnectButton()
        {
            this.connectButton.Text = "Connected";
            // The connectButton is disabled
            this.connectButton.Enabled = false;
        }
 
    }
}
